Version Scheme
--------------
ScPrime uses the following versioning scheme, vX.X.X.X
 - First Digit signifies a major (compatibility breaking) release
 - Second Digit signifies a major (non compatibility breaking) release
 - Third Digit signifies a minor release
 - Fourth Digit signifies a patch release

Version History
---------------

Latest:

### v1.4.2.0
**Key Updates**
 - Smarter fund allocation when initially forming contracts
 - When repairing files from disk, an integrity check is performed to ensure
   that corrupted / altered data is not used to perform repairs

Sep 2019:
